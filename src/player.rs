extern crate reqwest;
extern crate image;

use reqwest::{Client, Url};

enum media_type {
	Song,
	Podcast
}

enum repeat_mode {
	Off,
	Track,
	Context
}

enum shuffle_mode {
	On,
	Off
}

struct media_info {
	is_playing: bool,
	song_name: String,
	artist_name: String,
	media_type: String, //todo change to media type enum or something
	duration: i32,	//milliseconds
	progress: i32,	//milliseconds
	album_image: image::ImageResult //todo album_image type
}

pub fn get_currently_playing_info(client: &Client) -> media_info {}

pub fn play_pause(client: &Client) {}

pub fn play(client: &Client) {}

pub fn pause(client: &Client) {}

pub fn previous(client: &Client) {}

pub fn next(client: &Client) {
	let url = Url::parse(&"https://api.spotify.com/v1/me/player/next").unwrap();

	let request = client
		.post(url)
		.header("Authorization", reqwest::header::HeaderValue::from_static("Bearer BQD9hicwVneFVVlBJyH1ONF-fgA7l-nqVN5VRYDU_rIvEBFqz6tqbDsPC1Au1F6Njz3ygM9IrdGS34ItQtXDXrSpfGsTx2KpUWKbC_ct_SMUjC8EFyxpuyRoIVw4XLg2vc460MHzWHl_nN__4494frRQz_GthcEyU26NgHTjZQ"))
		// .header(reqwest::header::CONTENT_LENGTH, "1");
		.body("");

	let mut result = request.send().unwrap();

	println!("{}", result.text().unwrap());
}

pub fn seek(client: &Client, seek_location: i32) {}

pub fn set_volume(client: &Client, volume: i32) {}

pub fn set_repeat_mode(client: &Client, repeat_mode: repeat_mode) {}

pub fn set_shuffle_mode(client: &Client, shuffle_mode: shuffle_mode) {}