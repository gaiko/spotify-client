extern crate reqwest;
extern crate webbrowser;

use reqwest::{Client, Url, get};


const code: &'static str = "AQD9_P_Xf2Ag4zkPSl-qzmTJWhxS50gvdhTfKSzCCBM-Dx3RohPzXG_SPnWe8Zi2jk2tDsBRwIs9K-xDh71CYksjP7i9nb4SdMi7HkOE2tYZJxDzKHVZp1Td2SshN53M7cVXJXadr0KiN5GGZ9pa63NEkR7cohzMJpvTujgw4soD9AJXqaPp12IGkx2EWg2qTVPheHkQBP1NnvMwlNo-cGhSfQ";

const CLIENT_ID: &'static str = "a09583f9a64b46868f00048977f6e4b5";
const CLIENT_SECRET: &'static str = "7a67591938ea4279866f9fffb7624fa9";
const REDIRECT_URI: &'static str = "https://www.example.com";
const SCOPE: &'static str = "app-remote-control";

pub fn get_authorization_code(client: &Client) {
	let url = Url::parse_with_params(
		"https://accounts.spotify.com/authorize",
		&[
			("response_type", "code"),
			("client_id", CLIENT_ID),
			("scope", SCOPE),
			("redirect_uri", REDIRECT_URI),
		],
	);

	let url = match url {
		Ok(result) => result,
		Err(message) => panic!(message),
	};

	let response = get(url);

	webbrowser::open(response.unwrap().url().as_str()).expect("Unable to open url");
}

pub fn get_access_token(client: &Client) {
	let url = Url::parse(&"https://accounts.spotify.com/api/token").unwrap();

	let request = client.post(url).form(&[
		("grant_type", "authorization_code"),
		("code", code),
		("redirect_uri", REDIRECT_URI),
		("client_id", CLIENT_ID),
		("client_secret", CLIENT_SECRET),
	]);

	let mut result = request.send().unwrap();
	let access_token = result.headers().get("access_token");
	let refresh_token = result.headers().get("refresh_token");
	println!("{}", result.text().unwrap());
	// println!("Access: {}\tRefresh: {}", access_token, refresh_token);
}

pub fn refresh_access_token(client: &Client, refresh_token: &str) {
	let url = Url::parse(&"https://accounts.spotify.com/api/token").unwrap();

	let request = client.post(url).form(&[
		("grant_type", "refresh_token"),
		("refresh_token", refresh_token),
		("client_id", CLIENT_ID),
		("client_secret", CLIENT_SECRET),
	]);

	let mut result = request.send().unwrap();

	let access_token = result.headers().get("access_token");
	let refresh_token = result.headers().get("refresh_token");

	println!("{}", result.text().unwrap());
}